class Report
	def initialize(formatter)
		@title = 'タイトル'
		@text = ['文章A', '文章B', '文章C']
		@formatter = formatter
	end

	attr_reader :title, :text
	attr_accessor :formatter

	def output_report
		@formatter.output_report( @title, @text)
	end
end

#class Formatter
#	def output_report(title,text)
#		raise 'Abstract method called'
#	end
#end

class HTMLFormatter
	def output_report(title,text)
		puts('<html>')
		puts('<head>')
		puts("<title>#{@title}</title>")
		puts('</head>')
		puts('<body>')

		text.each {|line|
		puts("<p>#{line}</p>")
		}

		puts('</body>')
		puts('</html>')
	end
end

class PlainTextFormatter
	def output_report(title,text)
		puts("#{title}")
		text.each{|line|
		puts("#{line}\n")
		}
	end
end

report = Report.new(HTMLFormatter.new)	#initializeに渡すのはインスタンス
report.output_report

report.formatter=(PlainTextFormatter.new)	#ストラテジーを交換
report.output_report
