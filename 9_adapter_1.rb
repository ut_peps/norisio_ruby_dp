class Client
	def initialize(object)
		@object =object
	end
	def execute
		@object.method
	end
end


class Adapter
	def initialize(object)
		@object = object
	end
	def method		#この部分で返り値等を適切に変形することができる。
		puts("アダプターが稼働し")
		@object.mesoddo
	end
end

class Adaptee
	def initialize
	end
	def mesoddo
		puts("実行されました")
	end
end

client = Client.new(Adapter.new(Adaptee.new))
client.execute
