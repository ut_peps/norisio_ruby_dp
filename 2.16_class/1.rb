class BankAccount
	def initialize( account_owner )
		@owner = account_owner
		@balance = 0
	end

	def deposit(amout)
		@balance= @balance + amout
	end

	def withdraw(amout)
		@balance= @balance - amout
	end

	attr_accessor :balance
end

my_account = BankAccount.new('tanaka')
puts(my_account.balance)
