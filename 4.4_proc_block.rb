def run_it(&block)		#& はブロックをProcオブジェクトに変換できる
	puts("Before the yield")
	block.call
	puts("After the yield")
end

run_it {
	puts ("Hello, Coming to you from inside the block")
}
