class Client
	def initialize(object)
		@object =object
	end
	def execute
		@object.execute
	end
	def another_method
		@object.another_method
	end
end

class Proxy
	def initialize(&blocktomakesubject)
		@object = nil
		@block = blocktomakesubject
	end
	def execute
		object.execute
	end
	def object
		if (@object == nil) 
			puts("Subjectオブジェクトが生成され")
			@object = @block.call
		end
		@object
	end
	def method_missing(name, *args)
		puts("何らかの処理を挟んだ上で")
		object.send(name, *args)
	end
end


class Subject
	def initialize
	end
	def execute
		puts("実行されました")
	end
	def another_method
		puts("another_methodも呼び出されました")
	end
end


proxy = Proxy.new do
	Subject.new
end
client = Client.new(proxy)
client.execute
client.another_method

