
norisio君が [Rubyによるデザインパターン](http://www.amazon.co.jp/Ruby%E3%81%AB%E3%82%88%E3%82%8B%E3%83%87%E3%82%B6%E3%82%A4%E3%83%B3%E3%83%91%E3%82%BF%E3%83%BC%E3%83%B3-Russ-Olsen/dp/4894712857)
を使ってRubyの基本, 動的型付け言語の良さ, およびオブジェクト志向の理念とかに触れておきたいというリポジトリです。

ひとまず通った・読んだ

* 3章 アルゴリズムを変更する: Template Method
* 4章 アルゴリズムを交換する: Strategy
* 5章 変更に追従する: Observer
* 6章 部分から全体を組み立てる: Composite
* 7章 コレクションを操作する: Iterator
* 8章 命令を実行する: Command
* 9章 ギャップを埋める: Adapter
* 10章 オブジェクトに代理を建てる: Proxy

これから

* 11章 オブジェクトを改良する: Decorator
* 12章 唯一を保証する: Singleton
* 13章 正しいクラスを選び出す: Factory
* 14章 オブジェクトを組み立て易くする: Builder
* 15章 専用の言語で組み立てる: Interpreter
* 16章 オリジナル言語を作る: Domain-Specific Languages(DSL)
* 17章 カスタムオブジェクトを作る: メタプログラミング
* 18章 Convention over Configuration(CoC)
