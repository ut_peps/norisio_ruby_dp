class Subject
	attr_reader :name, :title
	attr_reader :salary

	def initialize(name,title,salary)
		@name = name
		@title = title
		@salary = salary
		@observers = []
	end

	def salary=(new_salary)		#セッター
		@salary = new_salary
		notify_observers
	end

	def notify_observers
		@observers.each{|observer|
			observer.update(self)
		}
	end

	def add_observer(observer)
		@observers << observer
	end

	def delete_observer(observer)
		@observers.delete(observer)
	end
end

class Observer1
	def update(subject)
		puts("Observer1 catched a notification")
		puts("new salary is #{subject.salary}")
	end
end
class Observer2
	def update(subject)
		puts("Observer2 catched a notification")
		puts("new salary is #{subject.salary}")
	end
end

tanaka = Subject.new('Tanaka', 'Manager', 100000.0)
obs1 = Observer1.new
obs2 = Observer2.new
tanaka.add_observer(obs1)
tanaka.add_observer(obs2)
tanaka.salary=200000.0

