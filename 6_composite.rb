#
#6章 Composite
#ディレクトリ・ファイルのツリー構造を実装してみる
#

class Item			#Component
	def initialize(name)
		@name = name
	end

	def get_size
		raise 'Abstract function called'
	end

	def ls(level)
		raise 'Abstract function called'
	end
end

class MyFile < Item		#Leaf
	def initialize(name,size)
		super(name)
		@size = size
	end

	def get_size
		@size
	end

	def ls(level=0)
		for i in 0..level
			print "-"
		end
		print "#{@name} (#{@size} KB)\n"
	end
end

class MyDirectory < Item	#Composite
	def initialize(name)
		super(name)
		@children = []
	end

	def add_child(file)
		@children << file
	end
	def del_child(file)
		@children.delete(file)
	end

	def get_size
		sum = 0
		@children.each do |child|
			sum += child.get_size
		end
		sum
	end
	
	def ls(level=0)
		for i in 0..level
			print "-"
		end
		print "#{@name} (DIR)\n"
		@children.each do |child|
			child.ls(level+1)
		end
	end
end

root = MyDirectory.new("root")
dir1 = MyDirectory.new("dir1")
dir2 = MyDirectory.new("dir2")
root.add_child(dir1)
root.add_child(dir2)
f_readme = MyFile.new("readme.md",1.0)
f_aout = MyFile.new("a.out",3.2)
f_ac = MyFile.new("a.c", 1.2)
dir1.add_child(f_readme)
dir1.add_child(f_aout)
dir1.add_child(f_ac)
f_indexhtml = MyFile.new("index.html",2.4)
dir2.add_child(f_indexhtml)

root.ls	#list表示

puts "The size of root directory is #{root.get_size} KB"	#dirのサイズを集計
puts "The size of dir1 is #{dir1.get_size} KB"

