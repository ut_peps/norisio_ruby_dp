module HelloModule
	def say_hello
		puts('Hello out there.')
	end
end


class TryIt
	include HelloModule
end

instance = TryIt.new
instance.say_hello		#->Hello out there.
