class Report
	def initialize(&formatter)		#ブロックを受け取ってProcオブジェクトとして保持
		@title = 'タイトル'
		@text = ['文章A', '文章B', '文章C']
		@formatter = formatter
	end

	attr_reader :title, :text
	attr_accessor :formatter

	def output_report
		@formatter.call(self)		#呼び出せるメソッドはcallしかない
	end
end

HTMLFORMATTER = lambda do |context|
	puts('<html>')
	puts('<head>')
	puts("<title>#{context.title}</title>")
	puts('</head>')
	puts('<body>')

	context.text.each {|line|
		puts("<p>#{line}</p>")
	}

	puts('</body>')
	puts('</html>')
end

report = Report.new &HTMLFORMATTER	#&をつけてProcオブジェクトをブロックとして渡す
report.output_report

